#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Create $DIR/tmp
TMP=$DIR/tmp

mkdir $TMP

# Update cache
sudo apt-get update

# Install VirtualBox Guest Additions
[ "$1" = "-g" ] && sudo apt-get install -y virtualbox-guest-dkms virtualbox-guest-utils virtualbox-guest-x11

# Install essential packages
sudo apt-get install -y arc-theme astyle
sudo apt-get install -y chromium-browser cmatrix curl
sudo apt-get install -y dkms
sudo apt-get install -y fzf
sudo apt-get install -y git
sudo apt-get install -y htop
sudo apt-get install -y language-pack-en language-pack-pt
sudo apt-get install -y mercurial
sudo apt-get install -y openjdk-11-jdk openssh-server
sudo apt-get install -y python3 python3-dev python3-pip
sudo apt-get install -y tree
sudo apt-get install -y vlc
sudo apt-get install -y xsel
sudo apt-get install -y zsh
sudo apt-get install -y vim vim-gui-common vim-runtime
sudo apt-get install -y ubuntu-restricted-extras

# Install VirtualBox
[ "$1" != "-g" ] && sudo apt-get install -y virtualbox virtualbox-dkms

# Install Oh My Zsh
git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh

# Install pip packages
sudo pip3 install -U setuptools requests virtualenv

# Install dotfiles
git clone https://tgomes95@bitbucket.org/tgomes95/dotfiles.git $TMP/dotfiles

cat $DIR/dotfiles/bashrc >> $HOME/.bashrc

cp $DIR/dotfiles/bash_aliases $HOME/.bash_aliases
cp $DIR/dotfiles/bash_functions $HOME/.bash_functions
cp $DIR/dotfiles/fzf $HOME/.fzf
cp $DIR/dotfiles/zshrc $HOME/.zshrc

cp $TMP/dotfiles/astylerc $HOME/.astylerc
cp $TMP/dotfiles/gitconfig $HOME/.gitconfig
cp $TMP/dotfiles/hgrc $HOME/.hgrc

# Install scripts
git clone https://tgomes95@bitbucket.org/tgomes95/scripts.git $HOME/.bin
rm -rf $HOME/.bin/.git* $HOME/.bin/install

# Move wallpapers to Pictures
cp $DIR/pictures/* $HOME/Pictures

# Configure GRUB
cat $DIR/grub/grub | sudo tee -a /etc/default/grub > /dev/null
sudo update-grub

# General configurations
sudo tee -a /etc/sysctl.d/99-sysctl.conf <<-EOF

# Custom configurations

vm.dirty_background_ratio=5
vm.dirty_ratio=10

vm.swappiness=10

vm.vfs_cache_pressure=50
EOF

# Configure ssh
sudo ufw allow 22
ssh-keygen

# Configure zsh
cp -r $DIR/dotfiles/oh-my-zsh/* $HOME/.oh-my-zsh/
chsh -s $(which zsh)

# Upgrade and clean
sudo apt-get upgrade -y
sudo apt-get autoclean
sudo apt-get autoremove -y

# Remove $DIR/tmp
rm -rf $TMP

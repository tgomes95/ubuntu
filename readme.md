# Ubuntu

Installation scripts for Ubuntu

## Instructions

### Run install.sh for host machines


```
#!bash

./install.sh
```

### Run guest/install.sh for guest machines


```
#!bash

cd guest/
./install.sh
```

### Run dotfiles/install.sh to install only the dotfiles


```
#!bash

cd dotfiles/
./install.sh
```

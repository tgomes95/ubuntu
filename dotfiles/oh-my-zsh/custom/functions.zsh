#$ZSH_CUSTOM/functions.zsh

# Extract
function extract
{
    if [ -f "$1" ]; then
        case "$1" in
            *.7z)      7z x "$1"       ;;
            *.Z)       uncompress "$1" ;;
            *.bz2)     bunzip2 "$1"    ;;
            *.gz)      gunzip "$1"     ;;
            *.jar)     jar xf "$1"     ;;
            *.rar)     rar x "$1"      ;;
            *.tar)     tar xvf "$1"    ;;
            *.tar.bz2) tar xvjf "$1"   ;;
            *.tar.gz)  tar xvzf "$1"   ;;
            *.tbz2)    tar xvjf "$1"   ;;
            *.tgz)     tar xvzf "$1"   ;;
            *.zip)     unzip "$1"      ;;
            *)         echo "Unable to extract '$1'" ;;
        esac
    else
        echo "'$1' not found!"
    fi
}

# Content
function co
{
    cat $@ | pbcopy
}

# Content
function cs
{
    cat ${@:2} | grep -i "$1"
}

# Find
function ff
{
    find . -iname "*$1*" | grep -i "$1"
}

# Find
function fd
{
    find . -type d -iname "*$1*" | grep -i "$1"
}

# Find
function fe
{
    find . -iname "*$1*" -print -exec ${@:2} {} \; | grep -i "$1"
}

# Find
function fr
{
    find . -iname "*$1*" -delete -print | grep -i "$1"
}

# History
function hs
{
    if [ $# -eq 0 ]; then
        history | less +G
    else
        history | grep --color=always "$1" | less +G
    fi
}

# List
function le
{
    find . -type f -name "*.*" | awk -F. '{print $NF}' | sort -u
}

# Make
function mkcd
{
    mkdir -p $@ && cd ${@:$#}
}

# Permission
function pm
{
    stat -c '%a %n' $@
}

# Python
function pycl
{
    find . -type f -name "*.py[co]" -delete -o -type d -name __pycache__ -delete
}

# Reload
function rl
{
    [ -f $HOME/.zshrc ] && source $HOME/.zshrc
}

# Upgrade
function up
{
    sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get autoclean
    sudo PIP_REQUIRE_VIRTUALENV=false pip3 install -U pip setuptools requests virtualenv
}

# Virtualenv
function vr
{
    local VR=$(dirname $VIRTUAL_ENV)/requirements.txt

    ! [ -z $VIRTUAL_ENV ] && [ -f $VR ] && pip install -r $VR
}

# Virtualenv
function vv
{
    if [ -z $VIRTUAL_ENV ]; then
        ! [ -d venv ] && virtualenv venv
        source venv/bin/activate
        [ -f requirements.txt ] && pip install -r requirements.txt
    else
        deactivate
    fi
}

# Work
function ws
{
    [ -f $WORK/.workrc ] && source $WORK/.workrc && cd $WORK
}

# Zip
function zp
{
    zip -r "${1%%/}.zip" "$1"
}
